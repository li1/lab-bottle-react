from bottle import Bottle, request, redirect, response  
from bottle.ext.mongo import MongoPlugin
from bson.json_util import dumps

db_user = 'leoisso'
db_pass = 'leoisso07'
db_url = f'mongodb://{db_user}:{db_pass}@ds041924.mlab.com:41924/dev-leonardo'
db_name = "dev-leonardo"

app = Bottle()
plugin = MongoPlugin(uri=db_url,
                    db=db_name,
                    json_mongo=True)
app.install(plugin)

@app.hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

@app.route('/api/autores/', method='GET')
def get_autores(mongodb):
    return dumps(mongodb['autores'].find())

@app.route('/api/autores/', method='POST')
def add_autores(mongodb):
    try:
        nome_autor = request.POST.get('nome')
        email_autor = request.POST.get('email')
        senha_autor = request.POST.get('senha')
        
        dictionary = {'nome':nome_autor, 'email':email_autor, 'senha':senha_autor}
        mongodb['autores'].insert(dictionary)
    except Exception as err:
        return {'sucess': False, 'error': str(err)}

    return {'sucess': True}
    
@app.route('/api/livros/', method='GET')
def get_livros(mongodb):
    return dumps(mongodb['livros'].find())

@app.route('/api/livros/', method='POST')
def add_livros(mongodb):
    try:
        titulo_livro = request.POST.get('titulo')
        preco_livro = request.POST.get('preco')
        autor_livro = request.POST.get('autor')

        dictionary = {'titulo':titulo_livro, 'preco':preco_livro, 'autor':autor_livro}
        mongodb['livros'].insert(dictionary)
    except Exception as err:
        return {'sucess': False, 'error': str(err)}

    return {'sucess': True}

if __name__ == '__main__':
    app.run(debug=True, reloader=True)